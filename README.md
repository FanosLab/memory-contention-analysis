# Memory-Contention-Analysis

This is the source code that provides the formal modeling of memory contention in heterogenous COTS SoCs. 
The framework is represented as an ILP formulation that takes into account avaiable information about the COTS architecture, and contending applicaions to tighten memory interference 
latency bounds. 
It conducts a hybrid analysis that blends both Req-Dr and Job-Dr constraints. 

Please feel free to use this code and cite our ECRTS paper (which can be downloaded from [HERE](https://www.ece.mcmaster.ca/faculty/hassan/assets/publications/hassan_20b_ecrts.pdf)):

@inproceedings{hassan_20b_ecrts,

  author = {Hassan, Mohamed and Pellizzoni, Rodolfo},
  
  title = {Analysis of Memory-Contention in Heterogeneous COTS MPSoCs},
  
  booktitle = {{Euromicro Conference on Real-Time Systems  (ECRTS)}},
  
  year = {2020},
  
  pages = {1--22},
  
  month = jul,
  
  pdf = {/assets/publications/hassan_20b_ecrts.pdf}
  
}

This framework considers the exploration of 144 platform instances with the combination resulting from considered configurations. 
Below are some examples of these configurations:

## Core Pipeline Architecture
- In-order
- Out-of-Order
- Heterogeneous: Some cores are OoO and some are IO (for instance the critical cores can be IO while non-critical are OoO)

## OS DRAM Bank Partitioning
- Full partitioning: each core has its own bank(s)
- No Partitioning: all cores access all banks
- Heterogeneous Mix: where some cores are limited to certain banks while others cam access all

## Prioritization
- No priority: all cores have same priroity
- priority: some cores (critical) are given a higher priroity upon accessing the shared memory
- 

## Memory Controller Inter-Bank (FR-FCFS) Reordering
- FR-FCFS with no threshold
- FR-FCFS with threshold

## Read/Write Reorder
- No Read/Write Reorder: Reads and Writes are treated equally
- Write batching: writes are queued and scheduled differently than reads


# Source Code Files
The code has two files:

## DRAMopt.m
this is the general file with all the constraints and cases
## DRAMopt\_unboundedByReqDr\_cases.m
this file considers the sepcial case where the ReqDr analysis cannot provide analytical bounds on the memory
interference. So, we only consider the jobDr constraints. Below are these cases:

- when there is *no FR-FCFS* threshold and there is *no* bank paritioning deployed
- when there is *no FR-FCFS* threshold and there is *no* priroity and bank paritioning is only paritioning amond the critical cores
- when there is no R/W reorder and the inter-bank reordering is unlimited

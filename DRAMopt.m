numCores=4;
numCr=2;
numNCr=2;

%timing constraints
tREFI=5206;
tRFC=74;
Texec=18332164;
Texec_tREFI=3522;
tCCD=4;
tWL=9;
tB=4;
tWTR=5;
tRTW=7;
tRL=10;
tRTRS=1;
tRP=10;
tRAS=24;
tRCD=10;
tWR=10;
NB=8;
Nthr=8;
Wbtch=16;
tFAW=20;
tRC=34;
NBcr=4;
tRRD=4;

PR=4;

MAX_NUM = 10000000;
%flag to decide whether a p is cr (1) or ncr (0)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%these are variables for the features
%========================================
%wb=0;
%pr=0;
fileID = fopen('result_H_H5.txt','a');

        
for wb= 0:1
fprintf(fileID,"write batching now is %d\n", wb);
for pr= 0:1
for part =1:3 % 1:partAll, 2: partCr, 3:noPart

	part_All=0;
	part_Cr=0;
	if part == 1
		part_All=1;
	elseif part ==2
		part_Cr=1;
	end
	for pipe = 1:3 %1: IO_All, 2: IO_Cr, 3: OOO_All
	
		IO_All=0;
		IO_Cr=0;
		OOO_All=0;
		if pipe==1
			IO_All=1;
		elseif pipe==2
			IO_Cr=1;
		else
			OOO_All=1;
		end
		
		
		if part_All==1
			NB_p=[2 2 2 2];
		elseif part_Cr==1
			NB_p=[4 4 8 8];	
        else
            NB_p=[8 8 8 8];
		end	
		NB_i=NB_p(1);
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
		%H_R_o, H_R_c_p,..etc are ones computed in isolation
		%then the following variables are the ones in the actual schedule
		%this is high_high
		  H_R_o_p=[171194 39179 34878 98661];
		  H_R_c_p=[109000 62299 64000 66561];
		  H_W_o_p=[16369 39956 36449 8851];
		  H_W_c_p=[22214 37278 37364 13000];
		
		%this is high_low
		%H_R_o_p=[171194 242 225 242];
		%H_R_c_p=[109000 2000 2000 2000];
		%H_W_o_p=[16369 148 150 147];
		%H_W_c_p=[22214 331 328 331];
		
		%this is low_low
		 %H_R_o_p=[202 242 225 242];
		 %H_R_c_p=[2000 2000 2000 2000];
		 %H_W_o_p=[150 148 150 147];
		 %H_W_c_p=[332 331 328 331];
		
		%this is low_high
		 %H_R_o_p=[202 171194 98661 39179 ];
		 %H_R_c_p=[2000 109000 66561 62299];
		 %H_W_o_p=[150 16369 8851 39956];
		 %H_W_c_p=[332 22214 13000 37278];
		
		%this is low_mix1
		% H_R_o_p=[202 242 98661 39179 ];
		% H_R_c_p=[2000 2000 66561 62299];
		% H_W_o_p=[150 148 8851 39956];
		% H_W_c_p=[332 331 13000 37278];
		
		%this is low_mix2
		% H_R_o_p=[202 242 225 39179 ];
		% H_R_c_p=[2000 2000 2000 62299];
		% H_W_o_p=[150 148 150 39956];
		% H_W_c_p=[332 331 328 37278];
		
		%this is high_mix1
		% H_R_o_p=[171194 242 98661 39179];
		% H_R_c_p=[109000 2000 66561 62299];
		% H_W_o_p=[16369 148 8851 39956];
		% H_W_c_p=[22214 331 13000 37278];
		
		%this is high_mix2
		% H_R_o_p=[171194 242 225 39179];
		% H_R_c_p=[109000 2000 2000 62299];
		% H_W_o_p=[16369 148 150 39956];
		% H_W_c_p=[22214 331 328 37278];
		
		
		%for Core under analysis (always define it as p=0)
		H_R_o_i=H_R_o_p(1);
		H_R_c_i=H_R_c_p(1);
		H_W_o_i=H_W_o_p(1);
		H_W_c_i=H_W_c_p(1);
		H_i=H_R_o_i+H_R_c_i+H_W_o_i+H_W_c_i;
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		L_WB = optimvar('L_WB','LowerBound',0);
		L_Conf = optimvar('L_Conf','LowerBound',0);
		L_ACT = optimvar('L_ACT','LowerBound',0);
		L_PRE = optimvar('L_PRE','LowerBound',0);
		L_CAS = optimvar('L_CAS','LowerBound',0);
		L_self = optimvar('L_self','LowerBound',0);
		prob = optimproblem('ObjectiveSense','maximize');
		%prob.Objective = (L_Conf+L_PRE+L_ACT+L_CAS+(wb)*L_WB- (H_i-1)*tCCD); %30
		prob.Objective = (L_Conf+L_ACT+L_CAS+(wb)*L_WB - L_self);
		%prob.Objective = (L_ACT+L_Conf+L_PRE-L_self);
		
		x_CAS_WR = optimvar('x_CAS_WR','LowerBound',0);
		x_CAS_RW = optimvar('x_CAS_RW','LowerBound',0);
		
		R_InterB_CAS = optimvar('R_InterB_CAS','LowerBound',0);
		W_InterB_CAS = optimvar('W_InterB_CAS','LowerBound',0);
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%those are arrays as they are defined per core
		R_o_p = optimvar('R_o_p',1,numCores,'LowerBound',0,'UpperBound',H_R_o_p);
		W_o_p = optimvar('W_o_p',1,numCores,'LowerBound',0,'UpperBound',H_W_o_p);
		
		
		R_o_i = optimvar('R_o_i','LowerBound',0,'UpperBound',H_R_o_i);
		W_o_i = optimvar('W_o_i','LowerBound',0,'UpperBound',H_W_o_i);
		
		
		
		if (part_All == 1 && wb == 0)
		       H_c_cond=1;
		else
		       H_c_cond=MAX_NUM;
		end
		
		R_c_p = optimvar('R_c_p',1,numCores,'LowerBound',0,'UpperBound',H_c_cond*H_R_c_p);
		
		W_c_p = optimvar('W_c_p',1,numCores,'LowerBound',0,'UpperBound',H_c_cond*H_W_c_p);
		
		R_c_i = optimvar('R_c_i','LowerBound',0,'UpperBound',H_c_cond*H_R_c_i);
		W_c_i = optimvar('W_c_i','LowerBound',0,'UpperBound',H_c_cond*H_W_c_i);
		
		
		prob.Constraints.cons3 = R_c_p + R_o_p == H_R_c_p+H_R_o_p;
		prob.Constraints.cons4 = W_c_p + W_o_p == H_W_c_p+H_W_o_p;
		
		prob.Constraints.cons5 = R_c_i + R_o_i == H_R_c_i+H_R_o_i;
		prob.Constraints.cons6 = W_c_i + W_o_i == H_W_c_i+H_W_o_i;
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
		%IV. MEMORY DELAY ANALYSIS
		%%Interfering Requests
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		N_IntraB_c = optimvar('N_IntraB_c','LowerBound',0);
		N_IntraB_o = optimvar('N_IntraB_o','LowerBound',0);
		R_IntraB = optimvar('R_IntraB','LowerBound',0);
		W_IntraB = optimvar('W_IntraB','LowerBound',0);
		x_ConfW = optimvar('x_ConfW','LowerBound',0);
		N_WB = optimvar('N_WB','LowerBound',0);
		
		x_Conf = optimvar('x_Conf','LowerBound',0);
		x_CAS = optimvar('x_CAS','LowerBound',0);
		
		
		
		
		R_Conf_c = optimvar('R_Conf_c','LowerBound',0);
		W_Conf_c = optimvar('W_Conf_c','LowerBound',0);
		R_Reorder_o = optimvar('R_Reorder_o','LowerBound',0);
		W_Reorder_o = optimvar('W_Reorder_o','LowerBound',0);
		R_InterB_c__c = optimvar('R_InterB_c__c','LowerBound',0);
		W_InterB_c__c = optimvar('W_InterB_c__c','LowerBound',0);
		R_InterB_o__c = optimvar('R_InterB_o__c','LowerBound',0);
		W_InterB_o__c = optimvar('W_InterB_o__c','LowerBound',0);
		R_InterB_o = optimvar('R_InterB_o','LowerBound',0);
		W_InterB_o = optimvar('W_InterB_o','LowerBound',0);
		
		
		
		W_before = optimvar('W_before','LowerBound',0);
		W_after = optimvar('W_after','LowerBound',0);
		W_btch = optimvar('N_btch','LowerBound',0);
		
		R_Conf_c_p = optimvar('R_Conf_c_p',1,numCores,'LowerBound',0);
		W_Conf_c_p = optimvar('W_Conf_c_p',1,numCores,'LowerBound',0);
		R_Reorder_o_p = optimvar('R_Reorder_o_p',1,numCores,'LowerBound',0);
		W_Reorder_o_p = optimvar('W_Reorder_o_p',1,numCores,'LowerBound',0);
		R_InterB_o__c_p = optimvar('R_InterB_o__c_p',1,numCores,'LowerBound',0);
		R_InterB_c__c_p = optimvar('R_InterB_c__c_p',1,numCores,'LowerBound',0);
		W_InterB_o__c_p = optimvar('W_InterB_o__c_p',1,numCores,'LowerBound',0);
		W_InterB_c__c_p = optimvar('W_InterB_c__c_p',1,numCores,'LowerBound',0);
		R_InterB_o_p = optimvar('R_InterB_o_p',1,numCores,'LowerBound',0);
		W_InterB_o_p = optimvar('W_InterB_o_p',1,numCores,'LowerBound',0);
		
		
		W_after_p = optimvar('W_after_p',1,numCores,'LowerBound',0);
		W_before_p = optimvar('W_before_p',1,numCores,'LowerBound',0);
		W_btch_p = optimvar('W_btch_p',1,numCores,'LowerBound',0);
		
		
		
		%this is to say if we have WB --> write effect on other components is ZERO
		if wb ==1
		    wb_effect=0;
		else
		    wb_effect=MAX_NUM;
		end
		   
		    prob.Constraints.cons7 = W_Conf_c_p           <=wb_effect;
		    prob.Constraints.cons8 = W_Reorder_o_p        <=wb_effect;
		    prob.Constraints.cons9 = W_InterB_o__c_p      <=wb_effect;
		    prob.Constraints.cons10 = W_InterB_c__c_p     <=wb_effect;
		    prob.Constraints.cons11 = W_InterB_o_p  	  <=wb_effect;
		
		
		 
		
		prob.Constraints.cons12 = R_Conf_c          ==sum(R_Conf_c_p(2:end));
		prob.Constraints.cons13 = W_Conf_c          ==sum(W_Conf_c_p(2:end));
		prob.Constraints.cons14 = R_Reorder_o       ==sum(R_Reorder_o_p(2:end));
		prob.Constraints.cons15 = W_Reorder_o       ==sum(W_Reorder_o_p(2:end));
		prob.Constraints.cons16 = R_InterB_c__c      ==sum(R_InterB_c__c_p(2:end));
		prob.Constraints.cons17 = W_InterB_c__c      ==sum(W_InterB_c__c_p(2:end));
		prob.Constraints.cons18 = R_InterB_o__c    ==sum(R_InterB_o__c_p(2:end));
		prob.Constraints.cons19 = W_InterB_o__c    ==sum(W_InterB_o__c_p(2:end));
		prob.Constraints.cons20 = R_InterB_o         ==sum(R_InterB_o_p(2:end));
		prob.Constraints.cons21 = W_InterB_o         ==sum(W_InterB_o_p(2:end));
		prob.Constraints.cons22 = W_before        ==sum(W_before_p);
		prob.Constraints.cons23 = W_btch          ==sum(W_btch_p);
		prob.Constraints.cons24 = W_after         ==sum(W_after_p);
		
		
		
		prob.Constraints.cons25 = x_Conf  +x_CAS       <= R_Conf_c+W_Conf_c+R_Reorder_o+W_Reorder_o; %10
		prob.Constraints.cons26 = x_Conf               <= R_Conf_c+W_Conf_c+R_c_i+(1-wb)* W_c_i; %11
		%prob.Constraints.cons25 = x_Conf  +x_CAS       <= R_Reorder_o+W_Reorder_o; %11
		%prob.Constraints.cons26 = x_Conf               <= R_c_i+(1-wb)* W_c_i; %12
		
		N_InterB_c__PRE_c = optimvar('N_InterB_c__PRE_c','LowerBound',0);
		N_InterB_c__ACT_c = optimvar('N_InterB_c__ACT_c','LowerBound',0);
		W_InterB_c__CAS_c = optimvar('W_InterB_c__CAS_c','LowerBound',0);
		R_InterB_c__CAS_c = optimvar('R_InterB_c__CAS_c','LowerBound',0);
		
		
		prob.Constraints.cons27 = N_InterB_c__PRE_c + N_InterB_c__ACT_c + R_InterB_c__CAS_c + W_InterB_c__CAS_c <= R_InterB_c__c+W_InterB_c__c; %12
		prob.Constraints.cons28 = N_InterB_c__PRE_c + N_InterB_c__ACT_c + R_InterB_c__CAS_c <= R_InterB_c__c; %13
		prob.Constraints.cons29 = N_InterB_c__PRE_c + N_InterB_c__ACT_c + W_InterB_c__CAS_c <= W_InterB_c__c; %14
		
		
		prob.Constraints.cons30 = R_InterB_CAS ==  R_InterB_o__c+R_InterB_c__CAS_c+R_InterB_o; %15
		prob.Constraints.cons31 = W_InterB_CAS ==  W_InterB_o__c+W_InterB_c__CAS_c+W_InterB_o; %16
		
		
		
		
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%B. Self-Interference
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		R_OtC_i = optimvar('R_OtC_i','LowerBound',0);
		W_OtC_i = optimvar('W_OtC_i','LowerBound',0);
		
		R_Conf_i = optimvar('R_Conf_i','LowerBound',0);
		W_Conf_i = optimvar('W_Conf_i','LowerBound',0);
		R_CAS_i = optimvar('R_CAS_i','LowerBound',0);
		W_CAS_i = optimvar('W_CAS_i','LowerBound',0);
		N_None_i = optimvar('N_None_i','LowerBound',0);
		N_ACT_i = optimvar('N_ACT_i','LowerBound',0);
		N_ACTa_i = optimvar('N_ACTa_i','LowerBound',0);
		N_ACTb_i = optimvar('N_ACTb_i','LowerBound',0);
		
		if (part_All==1 && wb ==0)
		   part_All_effect_on_OtC=0;
		else
		   part_All_effect_on_OtC=MAX_NUM;
		end
		prob.Constraints.cons32 = R_OtC_i <= part_All_effect_on_OtC; %19
		prob.Constraints.cons33 = W_OtC_i <= part_All_effect_on_OtC; %19
		
		prob.Constraints.cons34 = R_OtC_i <= H_R_o_i - R_o_i; %17
		prob.Constraints.cons35 = W_OtC_i <= H_W_o_i - W_o_i; %18
		
		
		
		
		prob.Constraints.cons36 = R_Conf_i+W_Conf_i <= R_OtC_i+(1-wb)*W_OtC_i; %20
		
		if NB_i == 1
		    NB_i_equal_1 =1;
		    N_ACT_effect = 0;
		else
		    NB_i_equal_1 = 0;
		    N_ACT_effect = MAX_NUM;
		end
		
		
		
		prob.Constraints.cons37 = N_None_i == NB_i_equal_1 * ( R_c_i-R_OtC_i+(1-wb)*(W_c_i-W_OtC_i)); %21
		prob.Constraints.cons38 = N_ACT_i == N_ACTa_i + N_ACTb_i; %22
		prob.Constraints.cons39 = N_ACTa_i <= R_OtC_i+(1-wb)*W_OtC_i; %23
		prob.Constraints.cons40 = N_ACTa_i +N_ACTb_i <= R_c_i+(1-wb)*W_c_i; %24
		prob.Constraints.cons41 = N_ACT_i  <= N_ACT_effect; %25
		
		prob.Constraints.cons42 = R_CAS_i <= W_Conf_c+W_Reorder_o+W_InterB_CAS; %26
		prob.Constraints.cons43 = W_CAS_i <= R_Conf_c+R_Reorder_o+R_InterB_CAS; %27
		
		prob.Constraints.cons44 = R_Conf_i+W_Conf_i+N_ACT_i+R_CAS_i+W_CAS_i+N_None_i<=R_c_i+R_o_i+(1-wb)*(W_c_i+W_o_i)-1; %28
		prob.Constraints.cons45 = R_Conf_i+N_ACT_i+R_CAS_i+N_None_i<=R_c_i+R_o_i; %29
		prob.Constraints.cons46 = W_Conf_i+N_ACT_i+W_CAS_i+N_None_i<=(1-wb)*(W_c_i+W_o_i); %30
		
		prob.Constraints.cons47 = L_self == (R_Conf_i+W_Conf_i+N_ACTb_i+R_CAS_i+W_CAS_i)*tCCD+N_ACTa_i*tRRD; %31
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%%%Delay Components%%%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		%C. conf
		prob.Constraints.cons48 = L_Conf <= x_ConfW *(tRCD+tWL+tB+tWR+tRP)+ (x_Conf+R_Conf_i+W_Conf_i - x_ConfW) *(tRAS+tRP); %33 except that WB is kept separate
		
		prob.Constraints.cons49 = x_ConfW          <=R_Conf_i+W_Conf_i+x_Conf; %34
		prob.Constraints.cons50 = x_ConfW          <=W_Reorder_o+W_Conf_i+W_Conf_c; %35
		
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%D. PRE and ACT
		
		
		
		prob.Constraints.cons51 = L_PRE <= 3 * N_InterB_c__PRE_c; %36
		prob.Constraints.cons52 = L_ACT <=  (N_InterB_c__ACT_c+N_ACT_i) * (tFAW/4 + 1); %37
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%E.CAS
		
		%prob.Constraints.cons53_3 = R_CAS_i <= R_Reorder_o + R_InterB_CAS;
		%prob.Constraints.cons53_4 = W_CAS_i <= W_Reorder_o + W_InterB_CAS;
		
		% prob.Constraints.cons53 = x_CAS_WR <= W_InterB_CAS+W_CAS_i+W_Conf_c+W_Reorder_o; %43
		% prob.Constraints.cons54 = x_CAS_WR <= R_InterB_CAS+R_c_i+R_o_i+R_Conf_c+R_Reorder_o; %43
		% prob.Constraints.cons55 = x_CAS_RW <= R_InterB_CAS+R_CAS_i+R_Conf_c+R_Reorder_o; %43
		% prob.Constraints.cons56 = x_CAS_RW <= W_InterB_CAS+(1-wb)*(W_c_i+W_o_i)+W_Conf_c+W_Reorder_o; %43
		% prob.Constraints.cons57 = x_CAS_WR+x_CAS_RW <=R_InterB_CAS+W_InterB_CAS+x_CAS+R_CAS_i+W_CAS_i; %43
		
		
		
		prob.Constraints.cons53 = x_CAS_WR <=  W_CAS_i+W_Conf_c+W_InterB_CAS+W_Reorder_o; %43
		prob.Constraints.cons55 = x_CAS_RW <=  R_CAS_i+R_Conf_c+R_InterB_CAS+R_Reorder_o; %43
		
		prob.Constraints.cons54 = x_CAS_WR <= R_c_i+R_o_i+R_Conf_c+R_InterB_CAS+R_Reorder_o; %43
		prob.Constraints.cons56 = x_CAS_RW <= (1-wb)*(W_c_i+W_o_i)+W_Conf_c+W_InterB_CAS+W_Reorder_o; %43
		
		prob.Constraints.cons57 = x_CAS_WR+x_CAS_RW <=x_CAS+R_CAS_i+W_CAS_i+R_InterB_CAS+W_InterB_CAS; %43
		
		
		prob.Constraints.cons58 = L_CAS <= x_CAS_WR * (tWL+tB+tWTR) + x_CAS_RW * tRTW + (x_CAS+R_CAS_i+W_CAS_i + R_InterB_CAS+W_InterB_CAS- x_CAS_WR - x_CAS_RW) * tCCD; %38
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		%L_WB
		prob.Constraints.cons59 = L_WB<=(W_before+W_after+W_btch)*(tRCD+tWL+tB+tWR+tRP) ; % this remains same (32)
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%L_Refresh
		%prob.Constraints.cons60 = L_Refresh<= (Texec_tREFI+1)*tRFC;
		
		
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%V. INTERFERENCE COMPUTATION
		%%A. Job-Driven Bounds
		
		
		prob.Constraints.cons61 = R_Conf_c_p + R_InterB_c__c_p <= R_c_p; %47
		prob.Constraints.cons62 = W_Conf_c_p + W_InterB_c__c_p <= W_c_p; %48
		
		prob.Constraints.cons63 = R_Reorder_o_p + R_InterB_o__c_p   <= R_o_p; % 49
		prob.Constraints.cons64 = W_Reorder_o_p + W_InterB_o__c_p   <= W_o_p; % 50
		
		prob.Constraints.cons65 = R_Conf_c_p + R_InterB_c__c_p + R_Reorder_o_p + R_InterB_o__c_p + R_InterB_o_p <= R_c_p+R_o_p; %51
		prob.Constraints.cons66 = W_Conf_c_p + W_InterB_c__c_p + W_Reorder_o_p + W_InterB_o__c_p + W_InterB_o_p <= W_c_p+W_o_p; %52
		
		prob.Constraints.cons67 = W_after_p + W_before_p + W_btch_p<= W_c_p+W_o_p; %53
		
		
		%%following are per-request constraints
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%B. conf
		%this is the other critical core (equation 54)
		n_conf_p=zeros(1,numCores);
		 if(part_All  == 1 || part_Cr == 1)
		    n_conf_p(1:numCr)=0;
		elseif (IO_Cr == 1 || IO_All == 1)
		    n_conf_p(1:numCr)=1;
		else
		    n_conf_p(1:numCr)=PR;
		end
		
		%these are the non-critical ones
		if(part_All  == 1)
		    n_conf_p(numCr+1:end)=0;
		elseif (IO_All == 1 || pr==1)
		    n_conf_p(numCr+1:end)=1;
		else
		    n_conf_p(numCr+1:end)=PR;
		end
		
		prob.Constraints.cons68 =R_Conf_c_p(2:end)+W_Conf_c_p(2:end)<=n_conf_p(2:end)*(R_c_i+(1-wb)*W_c_i); %55
		
		
		if (pr == 0)
		       ncr_cond=MAX_NUM;
		else
		       ncr_cond=1;
		end
		
		prob.Constraints.cons69 =sum(R_Conf_c_p(numCr+1:end)+W_Conf_c_p(numCr+1:end))<=ncr_cond*(R_c_i+(1-wb)*W_c_i); %56
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%C. Reorder
		if (part_All == 1 || part_Cr == 1)
		    part_effect_Reorder_cr=0;
		else
		     part_effect_Reorder_cr=MAX_NUM;
		end
		
		if (part_All == 1 || pr == 1)
		    part_effect_Reorder_Ncr=0;
		else
		    part_effect_Reorder_Ncr=MAX_NUM;
		end
		
		
		prob.Constraints.cons70 = R_Reorder_o_p(2:numCr) <=part_effect_Reorder_cr; %43
		prob.Constraints.cons71 = W_Reorder_o_p(2:numCr) <=part_effect_Reorder_cr; %43
		
		
		
		prob.Constraints.cons72 = R_Reorder_o_p(numCr+1:end) <= part_effect_Reorder_Ncr; %44
		prob.Constraints.cons73 = W_Reorder_o_p(numCr+1:end) <= part_effect_Reorder_Ncr; %44
		
		prob.Constraints.cons74 =sum(R_Reorder_o_p(2:end)+W_Reorder_o_p(2:end))<=Nthr*(R_c_i+(1-wb)*W_c_i); %45
		
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%D. InterB
		
		N_reqs_c = optimvar('N_reqs_c','LowerBound',0);
		N_reqs_o = optimvar('N_reqs_o','LowerBound',0);
		
		prob.Constraints.cons75 = N_reqs_c  == R_c_i+(1-wb)*W_c_i + R_Conf_c+W_Conf_c; %60
		prob.Constraints.cons76 = N_reqs_o == R_o_i+(1-wb)*W_o_i + R_Reorder_o+W_Reorder_o; %61
		
		prob.Constraints.cons77 =R_InterB_o__c_p(2:end)+R_InterB_c__c_p(2:end)+W_InterB_o__c_p(2:end)+W_InterB_c__c_p(2:end)<=(NB_p(2:end)).*N_reqs_c; %62
		
		prob.Constraints.cons78 =sum(R_InterB_o__c_p(numCr+1:end)+R_InterB_c__c_p(numCr+1:end)+W_InterB_o__c_p(numCr+1:end)+W_InterB_c__c_p(numCr+1:end))<=ncr_cond.*N_reqs_c; %65
		
		prob.Constraints.cons79 =sum(R_InterB_o__c_p(2:numCr)+R_InterB_c__c_p(2:numCr)+W_InterB_o__c_p(2:numCr)+W_InterB_c__c_p(2:numCr))<=(NBcr).*N_reqs_c; %63
		
		prob.Constraints.cons80 =sum(R_InterB_o__c_p(2:end)+R_InterB_c__c_p(2:end)+W_InterB_o__c_p(2:end)+W_InterB_c__c_p(2:end))<=(NB-1).*N_reqs_c; %64
		
		
		prob.Constraints.cons81 =R_InterB_o_p(2:end)+W_InterB_o_p(2:end)<=(NB_p(2:end)).*N_reqs_o; %62
		
		prob.Constraints.cons82 =sum(R_InterB_o_p(numCr+1:end)+W_InterB_o_p(numCr+1:end))<=ncr_cond.*N_reqs_o; %65
		
		prob.Constraints.cons83 =sum(R_InterB_o_p(2:numCr)+W_InterB_o_p(2:numCr))<=(NBcr).*N_reqs_o; %63
		
		prob.Constraints.cons84 =sum(R_InterB_o_p(2:end)+W_InterB_o_p(2:end))<=(NB-1).*N_reqs_o; %64
		
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%E. WBatch
		prob.Constraints.cons85 =sum(W_btch_p)<=Wbtch*(R_o_i+R_c_i); %50
		
		%this is 52
		n_after_p=zeros(1,numCores);
		if (IO_All == 1)
		   n_after_p(:,:)=1;
		elseif (IO_Cr ==1)
		    n_after_p(2)=1;
		    n_after_p(3:end)=PR;
		else
		    n_after_p(:,:)=PR;
		end
		
		prob.Constraints.cons86 =W_after_p(2:end)<=n_after_p(2:end).*(R_o_i+R_c_i); %51
		
		
		prob.Constraints.cons87 =sum(W_before_p(numCr+1:end))<=ncr_cond.*(R_o_i+R_c_i); %53
		
		part_cond(1:numCores)=MAX_NUM;
		if (part_All == 1)
		    part_cond(:,:)=1;
		    part_all_cond=1;
		elseif (part_Cr == 1)
		    part_cond(1:numCr)=1;
		    part_cond(numCr+1:end)=MAX_NUM;
		    part_all_cond=MAX_NUM;
		else
		    part_cond(:,:)=MAX_NUM;
		    part_all_cond=MAX_NUM;
		end
		
		prob.Constraints.cons88 =W_before_p<=(part_cond.*NB_p).*(R_o_i+R_c_i);%54
		
		prob.Constraints.cons89 =sum(W_before_p(2:numCr))<=part_cond(2:numCr).*(NBcr-1).*(R_o_i+R_c_i); %55
		
		prob.Constraints.cons90 =sum(W_before_p(2:end))<=part_all_cond*(NB-1)*(R_o_i+R_c_i);%56
		
		prob.Constraints.cons91 =sum(W_before_p)<=(Nthr-1)*(NB-1)*(R_o_i+R_c_i); %57
		% prob.Constraints.cons1 = x + y <= 2;
		% 
		% y = optimvar('y');
		% prob = optimproblem;
		% prob.Objective = -x - y/3;
		% prob.Constraints.cons1 = x + y == 1.5;
		% prob.Constraints.cons2 = x + y/4 <= 1;
		% prob.Constraints.cons3 = x - y <= 2;
		% prob.Constraints.cons4 = x/4 + y >= -1;
		% prob.Constraints.cons5 = x + y >= 1;
		% prob.Constraints.cons6 = -x + y <= 2;
		% 
		[sol,fval] = solve(prob);

		fprintf(fileID,"%d %d %d %d %d\n", wb,pr,part,pipe,fval);
		
		
	end
end
end
end
fclose(fileID);